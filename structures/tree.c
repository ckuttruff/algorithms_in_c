#include <stdio.h>

typedef struct Tree {
  char * type;
  void * data;
  struct Tree * parent;
  struct Tree * left;
  struct Tree * right;
} Tree;

void print(Tree n) {
  if(strncmp(n.type, "int", 3) == 0)
    printf("%d\n", *((int *) n.data));
  else if(strncmp(n.type, "string", 6) == 0)
    printf("%s\n", (char *) n.data);
}

int main(void) {
  int num = 3;
  Tree m = { .type = "string", .data = "foo",
	     .parent = NULL, .left = NULL, .right = NULL };
  print(m);

  Tree n = { .type = "int", .data = &num,
	     .parent = NULL, .left = NULL, .right = NULL };
  print(n);

  return 0;
}
