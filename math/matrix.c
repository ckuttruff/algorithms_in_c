#include <stdio.h>

typedef struct Matrix {
  // row and column dimensions m and n of an mxn matrix respectively
  int dim_row;
  int dim_col;
  int * matrix;
} Matrix;

Matrix matrix(int m, int n, int * matrix) {
  Matrix res;
  res.dim_row = m;
  res.dim_col = n;
  res.matrix = matrix;
  return res;
}

void print_matrix(Matrix m) {
  int i, j;
  for(i = 0; i < m.dim_row; ++i) {
    for(j = 0; j < m.dim_col; ++j)
      printf("%d, ", m.matrix[i*3+j]);
    puts("");
  }


}

int main(void) {
  int arr[] = {  1, 2, 3,
		 4, 5, 6,
		 7, 8, 9 };
  Matrix m = matrix(3, 3, arr);
  print_matrix(m);

  return 0;
}
