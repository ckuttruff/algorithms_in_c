int strings_equal(char * type, const char * expected) {
  return strncmp(type, expected, strlen(expected)) == 0;
}
